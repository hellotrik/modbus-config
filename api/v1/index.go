package v1

import (
	"github.com/gogf/gf/v2/frame/g"
)

type IndexReq struct {
	g.Meta `path:"/" method:"get"  tags:"ChatService" summary:"Home page"`
}
type IndexRes struct {
	g.Meta `mime:"text/html" type:"string" example:"<html/>"`
}

type ScanReq struct {
	g.Meta `path:"/scan" method:"post"  tags:"ChatService" summary:"Home page"`
	Ip     string
	Port   string
}
type ScanRes struct {
	g.Meta `mime:"text/html" type:"string" example:"<html/>"`
	Data   interface{}
}
type TestReq struct {
	g.Meta `path:"/connect" method:"post"  tags:"ChatService" summary:"Home page"`
	Ip     string
	Port   string
}
type EditReq struct {
	g.Meta `path:"/edit" method:"post"  tags:"ChatService" summary:"Home page"`
	Ip     string
	Ip3    string
	Port   string
}
