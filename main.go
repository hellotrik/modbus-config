package main

import (
	"github.com/gogf/gf/v2/os/gctx"
	"modbuscf/internal/cmd"
)

func main() {
	cmd.Main.Run(gctx.New())
}
